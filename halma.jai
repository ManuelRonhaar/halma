State :: struct(w: s32, h: s32, p: s32) {
    width  :: w;
    height :: h;
    pawn_num :: p;
    board: [height][width] s8;
    red_pawns: [p] Pawn;
    blue_pawns: [p] Pawn;
    turn := RED;
    turns := 0;

    #place red_pawns;
    pawns: [2*p] Pawn;

    #place red_pawns;
    pawns_by_color: [2][p] Pawn;
}

State_NP :: type_of(start_state);

Pawn :: struct {
    #as using position: Position;
    color: s8;
    index: s64;
}

Position :: struct {
    x: s32;
    y: s32;
}

Option :: struct {
    pawn: *Pawn;
    new_position: Position;
}

operator + :: (p1: Position, p2: Position) -> Position {
    p: Position = ---;
    p.x = p1.x + p2.x;
    p.y = p1.y + p2.y;
    return p;
}

operator * :: (position: Position, scalar: s32) -> Position {
    p: Position = ---;
    p.x = position.x * scalar;
    p.y = position.y * scalar;
    return p;
}

operator == :: (p1: Position, p2: Position) -> bool {
    return p1.x == p2.x && p1.y == p2.y;
}

INVALID : s8 : -2;
EMPTY   : s8 : -1;
RED     : s8 : 0;
BLUE    : s8 : 1;

I :: INVALID;
E :: EMPTY;
R :: RED;
B :: BLUE;

start_state :: #run generate_start_state();

generate_start_state :: () -> State(25, 17, 10) {
    state: State(25, 17, 10);
    state.board = .[  // upside down from game view
        .[I, I, I, I, I, I, I, I, I, I, I, I, R, I, I, I, I, I, I, I, I, I, I, I, I],
        .[I, I, I, I, I, I, I, I, I, I, I, R, I, R, I, I, I, I, I, I, I, I, I, I, I],
        .[I, I, I, I, I, I, I, I, I, I, R, I, R, I, R, I, I, I, I, I, I, I, I, I, I],
        .[I, I, I, I, I, I, I, I, I, R, I, R, I, R, I, R, I, I, I, I, I, I, I, I, I],
        .[E, I, E, I, E, I, E, I, E, I, E, I, E, I, E, I, E, I, E, I, E, I, E, I, E],
        .[I, E, I, E, I, E, I, E, I, E, I, E, I, E, I, E, I, E, I, E, I, E, I, E, I],
        .[I, I, E, I, E, I, E, I, E, I, E, I, E, I, E, I, E, I, E, I, E, I, E, I, I],
        .[I, I, I, E, I, E, I, E, I, E, I, E, I, E, I, E, I, E, I, E, I, E, I, I, I],
        .[I, I, I, I, E, I, E, I, E, I, E, I, E, I, E, I, E, I, E, I, E, I, I, I, I],
        .[I, I, I, E, I, E, I, E, I, E, I, E, I, E, I, E, I, E, I, E, I, E, I, I, I],
        .[I, I, E, I, E, I, E, I, E, I, E, I, E, I, E, I, E, I, E, I, E, I, E, I, I],
        .[I, E, I, E, I, E, I, E, I, E, I, E, I, E, I, E, I, E, I, E, I, E, I, E, I],
        .[E, I, E, I, E, I, E, I, E, I, E, I, E, I, E, I, E, I, E, I, E, I, E, I, E],
        .[I, I, I, I, I, I, I, I, I, B, I, B, I, B, I, B, I, I, I, I, I, I, I, I, I],
        .[I, I, I, I, I, I, I, I, I, I, B, I, B, I, B, I, I, I, I, I, I, I, I, I, I],
        .[I, I, I, I, I, I, I, I, I, I, I, B, I, B, I, I, I, I, I, I, I, I, I, I, I],
        .[I, I, I, I, I, I, I, I, I, I, I, I, B, I, I, I, I, I, I, I, I, I, I, I, I]
    ];
    state.red_pawns  = get_pawns_from_board(state, RED);
    state.blue_pawns = get_pawns_from_board(state, BLUE);

    return state;
}

get_pawns_from_board :: (state: State($w, $h, $p), color: s8) -> [p] Pawn {
    pawns: [p] Pawn;
    index := 0;
    for y : 0..h-1 {
        for x : 0..w-1 {
            if state.board[y][x] == color {
                pawn: Pawn;
                pawn.y = y;
                pawn.x = x;
                pawn.index = index;
                pawn.color = color;
                pawns[index] = pawn;
                index += 1;
            }
        }
    }

    return pawns;
}

get_pawn_from_position :: (state: State, position: Position) -> *Pawn {
    for * pawn : state.pawns  if pawn == position  return pawn;
    Basic.assert(false);
    return null;
}

can_move :: (state: State, pawn: *Pawn, new_position: Position) -> bool {
    if pawn == null  return false;
    return array_find(get_valid_movements(state, *pawn.position), new_position);
}

move_pawn :: (state: *State, pawn: *Pawn, new_position: Position) {
    Basic.assert(can_move(state, pawn, new_position));
    state.board[new_position.y][new_position.x] = state.board[pawn.y][pawn.x];
    state.board[pawn.y][pawn.x] = EMPTY;
    pawn.position = new_position;
    state.turn ^= 1;  // hack to switch between RED and BLUE
    state.turns += 1;
}

get_ai_move :: (state: *State($w, $h, $p)) -> (pawn: *Pawn, destination: Position) {

    forward :: (state: State_NP, option: Option) -> float {
        // steps to reach the outermost finish field
        steps_to_finish :: (position: Position, turn: s8) -> float {
            x_difference := abs(cast(float) (12-position.x));
            y_difference := ifx turn == RED then 16-position.y else position.y;
            return Basic.max(x_difference, cast(float) y_difference);
        }

        return steps_to_finish(option.pawn, state.turn) - steps_to_finish(option.new_position, state.turn);
    }

    standard_deviation :: (s: State_NP, o: Option) -> float {
        copy := copy_state(s);
        state := *copy;
        option: Option = ---;
        option.pawn = get_pawn_from_position(state, o.pawn.position);
        option.new_position = o.new_position;
        move_pawn(state, option.pawn, option.new_position);
        state.turn ^= 1;

        average := Vector2.{0.0, 0.0};
        for pawn: state.pawns_by_color[state.turn]  average += make_vector2(xx pawn.x, xx pawn.y);
        average.x /= cast(float) state.pawn_num;
        average.y /= cast(float) state.pawn_num;

        standard_deviation := 0.0;
        for pawn: state.pawns_by_color[state.turn]
            standard_deviation += Math.square(cast(float) pawn.x - average.x) + Math.square(Math.sqrt(3.0) * (cast(float) pawn.y - average.y));

        standard_deviation = Math.sqrt(standard_deviation / cast(float) state.pawn_num);
        return standard_deviation;
    }

    // forward :: (state: State_NP, option: Option) -> float {
    //     if state.turn == RED  return cast(float) (-option.pawn.y + option.new_position.y);
    //     else                  return cast(float) (option.pawn.y - option.new_position.y);
    // }

    total_centered :: (state: State_NP, option: Option) -> float {
        total := 0.0;
        for pawn: state.pawns  total += pawn.x;
        return -Math.abs(total-160);
    }

    big_jump_next_turn :: (s: State_NP, o: Option) -> float {
        copy := copy_state(s);
        state := *copy;
        option: Option = ---;
        option.pawn = get_pawn_from_position(state, o.pawn.position);
        option.new_position = o.new_position;

        move_pawn(state, option.pawn, option.new_position);
        state.turn ^= 1;
        options := get_valid_movements(state);
        max := 0.0;
        for option : options {
            value := forward(state, option);
            if value > max  max = value;
        }

        return max;
    }

    small_jump_opponent :: (s: State_NP, o: Option) -> float {
        copy := copy_state(s);
        state := *copy;
        option: Option = ---;
        option.pawn = get_pawn_from_position(state, o.pawn.position);
        option.new_position = o.new_position;

        move_pawn(state, option.pawn, option.new_position);
        options := get_valid_movements(state);
        max := 0.0;
        for option : options {
            value := forward(state, option);
            if value > max  max = value;
        }

        return -max;
    }

    options := get_valid_movements(state);
    //Basic.print("%\n", options);

    Value_Function :: #type (state: type_of(start_state), option: Option) -> float;
    //Value_Function :: type_of(forward);
    Function_Pair :: struct {
        function: Value_Function;
        weight: float;
    }

    //function_list := Function_Pair.[.{forward, 5}, .{stay_centered, 1}, .{big_jump_next_turn, 3}, .{small_jump_opponent, 2}];
    function_list := Function_Pair.[.{forward, 5}, .{standard_deviation, -6}, .{big_jump_next_turn, 3}, .{small_jump_opponent, 2}];

    best_option: Option;
    max_value := 0hFF800000;; // -inf

    if state.turns < 2 {
        if state.turn == RED
            return get_pawn_from_position(state, Position.{15, 3}), Position.{14, 4};
        else
            return get_pawn_from_position(state, Position.{15, 13}), Position.{14, 12};
    }

    for option: options {
        value := 0.0;
        for pair: function_list  value += pair.weight*pair.function(state, option);
        if value > max_value  best_option, max_value = option, value;
    }

    return best_option.pawn, best_option.new_position;
}

get_random_move :: (state: *State($w, $h, $p)) -> (pawn: *Pawn, destination: Position) {
    while true {
        pawn: *Pawn;

        // choose a pawn
        if state.turn == RED  pawn = *state.red_pawns[random_int(p)];
        else                  pawn = *state.blue_pawns[random_int(p)];
        Basic.print("pawn: %, possible moves: ", pawn);

        // get the possible moves
        possibilities := get_valid_movements(state, pawn);
        Basic.print("%\n", possibilities);
        if possibilities {
            chosen := possibilities[random_int(possibilities.count)];
            return pawn, chosen;
        }
    }
}

make_option :: (pawn: *Pawn, position: Position) -> Option {
    option: Option = ---;
    option.pawn = pawn;
    option.new_position = position;
    return option;
}

get_valid_movements :: (state: State) -> [] Option {
    options: [..] Option;

    // @codeduplication
    if state.turn == RED {
        for * pawn : state.red_pawns  {
            new_positions := get_valid_movements(state, pawn);
            for position : new_positions  Basic.array_add(*options, make_option(pawn, position));
        }
    } else if state.turn == BLUE {
        for * pawn : state.blue_pawns  {
            new_positions := get_valid_movements(state, pawn);
            for position : new_positions  Basic.array_add(*options, make_option(pawn, position));
        }
    }

    return options;
}

get_valid_movements :: (state: State, position: Position) -> [] Position {
    if state.turn != state.board[position.y][position.x] then return Position.[];

    directions := Position.[.{1, 1}, .{2, 0}, .{1, -1}, .{-1, -1}, .{-2, 0}, .{-1, 1}];
    new_positions: [..] Position;

    // jumping
    {
        Basic.array_add(*new_positions, position);
        defer Basic.array_unordered_remove_by_index(*new_positions, 0);

        index := 0;  // first index of not visited position
        while true {
            count := new_positions.count;
            if count == index  break;
            for i : index..count-1 {
                position := new_positions[i];
                for direction : directions {
                    new_position := position + direction*2;
                    jumping_over := position + direction;
                    if new_position.x < 0 || new_position.x >= state.width || new_position.y < 0 || new_position.y >= state.height  continue;
                    if state.board[new_position.y][new_position.x] == EMPTY && state.board[jumping_over.y][jumping_over.x] > EMPTY {
                        if !array_find(new_positions, new_position)
                            Basic.array_add(*new_positions, new_position);
                    }
                }
            }
            index = count;
        }
    }

    // stepping
    for direction : directions {
        new_position := position + direction;
        if new_position.x < 0 || new_position.x >= state.width || new_position.y < 0 || new_position.y >= state.height  continue;
        if state.board[new_position.y][new_position.x] == EMPTY {
            Basic.array_add(*new_positions, new_position);
        }
    }

    return new_positions;
}

get_route :: (state: State, pawn: *Pawn, new_position: Position) -> [] Position {

    l: [..] Position;
    Basic.assert(array_find(get_valid_movements(state, *pawn.position), new_position));
    // NOT EFFICIENT probably
    directions := Position.[.{1, 1}, .{2, 0}, .{1, -1}, .{-1, -1}, .{-2, 0}, .{-1, 1}];

    make_double_position :: (p1: Position, p2: Position) -> Double_Position {
        p: Double_Position = ---;
        p.orig = p1;
        p.dest = p2;
        return p;
    }

    Double_Position :: struct {
        orig: Position;
        dest: Position;
    }
    new_positions: [..] Double_Position;


    array_find_double_position :: (array: [] Double_Position, item: Position) -> bool, s64 {
        for array if it.dest == item return true, it_index;
        return false, -1;  // Not found.
    }

    // stepping
    for direction : directions {
        destination := pawn.position + direction;
        if destination.x < 0 || destination.x >= state.width || destination.y < 0 || destination.y >= state.height  continue;
        if state.board[destination.y][destination.x] == EMPTY {
            if destination == new_position {
                l : [..] Position;
                Basic.array_add(*l, pawn.position);
                Basic.array_add(*l, destination);
                return l;
            }
        }
    }

    // jumping
    {
        Basic.array_add(*new_positions, make_double_position(Position.{1,0}, pawn.position));
        defer Basic.array_unordered_remove_by_index(*new_positions, 0);

        index := 0;  // first index of not visited position
        while big := true {
            count := new_positions.count;
            if count == index  break;
            for i : index..count-1 {
                position := new_positions[i].dest;
                for direction : directions {
                    destination := position + direction*2;
                    jumping_over := position + direction;
                    if destination.x < 0 || destination.x >= state.width || destination.y < 0 || destination.y >= state.height  continue;
                    if state.board[destination.y][destination.x] == EMPTY && state.board[jumping_over.y][jumping_over.x] > EMPTY {
                        if !array_find_double_position(new_positions, destination)
                            Basic.array_add(*new_positions, make_double_position(position, destination));
                        if destination == new_position  break big;
                    }
                }
            }
            index = count;
        }

        position := new_position;

        while true {
            found, index := array_find_double_position(new_positions, position);
            if !found  break;
            Basic.array_insert_at(*l, position, 0);
            position = new_positions[index].orig;

        }
    }

    return l;
}

red_won :: (state: State) -> bool {
    for pawn : state.red_pawns  if pawn.y < 13  return false;
    return true;
}

blue_won :: (state: State) -> bool {
    for pawn : state.blue_pawns  if pawn.y > 3  return false;
    return true;
}

copy_state :: (state: State($w, $h, $p)) -> (State(w, h, p)) {
    s: State(w, h, p);
    memcpy(*s, *state, size_of(type_of(s)));
    return s;
}

array_copy :: (array: [$size] $T) -> [size] T {
    dest : [size] T;
    memcpy(dest.data, array.data, size * size_of(T));
    return dest;
}

array_find :: (array: [] $T, item: T) -> bool, s64 {
    for array if it == item return true, it_index;
    return false, -1;  // Not found.
}

random_int :: (exclusive_max: s64) -> s64 {
    f := Random.random_get_zero_to_one_open();
    return cast(int) (f * exclusive_max);
}







// It might be a good idea to get a function for steps till final position.
// position standard deviation might also be good

get_pws_move :: (state: *State($w, $h, $p)) -> (pawn: *Pawn, destination: Position) {
    z_value :: (position: Position, turn: s8) -> float {
        x_difference := Math.abs(cast(float) (12-position.x));
        y_difference := ifx turn == RED then 16-position.y else position.y;
        difference := x_difference - y_difference;
        if difference > 0
            return Math.square(0.5*y_difference + 0.5*x_difference + 5);
        else
            return Math.square(cast(float) (y_difference + 5));
    }

    zmovement :: (state: State_NP, option: Option) -> float {
        return z_value(option.pawn, state.turn) - z_value(option.new_position, state.turn);
    }

    lfc :: (state: State_NP, option: Option) -> float {
        return Math.abs(cast(float) (option.new_position.x - 12)) - Math.abs(cast(float) (option.pawn.x - 12));
    }

    tlfc :: (state: State_NP, option: Option) -> float {
        total := 0;
        for pawn: state.pawns_by_color[state.turn]
            total += pawn.x;
        return Math.abs(cast(float) (total - 120));
    }

    density_x :: (state: State_NP, option: Option) -> float {
        xtotal, ytotal := 0;
        for pawn: state.pawns_by_color[state.turn] {
            xtotal += pawn.x;
            ytotal += pawn.y;
        }
        xaverage := xx xtotal / 10.0;
        yaverage := xx ytotal / 10.0;

        xdense, ydense := 0.0;
        for pawn: state.pawns_by_color[state.turn] {
            xdense += Math.abs(cast(float) (pawn.x - xaverage));
            ydense += Math.abs(cast(float) (pawn.y - yaverage));
        }
        return xdense;
    }

    density_y :: (state: State_NP, option: Option) -> float {
        xtotal, ytotal := 0;
        for pawn: state.pawns_by_color[state.turn] {
            xtotal += pawn.x;
            ytotal += pawn.y;
        }
        xaverage := xx xtotal / 10.0;
        yaverage := xx ytotal / 10.0;

        xdense, ydense := 0.0;
        for pawn: state.pawns_by_color[state.turn] {
            xdense += Math.abs(cast(float) (pawn.x - xaverage));
            ydense += Math.abs(cast(float) (pawn.y - yaverage));
        }
        return ydense;
    }

    bpawn :: (state: State_NP, option: Option) -> float {
        last, second_last: int;
        if state.turn == RED {
            last, second_last = 20;
            for pawn: state.red_pawns {
                if pawn.y < last {
                    second_last = last;
                    last = pawn.y;
                } else if pawn.y < second_last
                    second_last = pawn.y;
            }
        } else {
            last, second_last = 0;
            for pawn: state.blue_pawns {
                if pawn.y > last {
                    second_last = last;
                    last = pawn.y;
                } else if pawn.y > second_last
                    second_last = pawn.y;
            }
        }
        return Math.abs(cast(float) (last- second_last));
    }

    ljnt :: (s: State_NP, o: Option) -> float {
        copy := copy_state(s);
        state := *copy;
        option: Option = ---;
        option.pawn = get_pawn_from_position(state, o.pawn.position);
        option.new_position = o.new_position;

        move_pawn(state, option.pawn, option.new_position);
        state.turn ^= 1;
        options := get_valid_movements(state);
        max := 0.0;
        for option : options {
            value := zmovement(state, option);
            if value > max  max = value;
        }

        return max;
    }

    tjnt :: (s: State_NP, o: Option) -> float {
        copy := copy_state(s);
        state := *copy;
        option: Option = ---;
        option.pawn = get_pawn_from_position(state, o.pawn.position);
        option.new_position = o.new_position;

        move_pawn(state, option.pawn, option.new_position);
        state.turn ^= 1;
        options := get_valid_movements(state);
        total := 0.0;
        for option : options {
            value := zmovement(state, option);
            if value > 0  total += value;
        }

        return total;
    }

    ljet :: (s: State_NP, o: Option) -> float {
        copy := copy_state(s);
        state := *copy;
        option: Option = ---;
        option.pawn = get_pawn_from_position(state, o.pawn.position);
        option.new_position = o.new_position;

        move_pawn(state, option.pawn, option.new_position);
        options := get_valid_movements(state);
        max := 0.0;
        for option : options {
            value := zmovement(state, option);
            if value > max  max = value;
        }

        return max;
    }

    tjet :: (s: State_NP, o: Option) -> float {
        copy := copy_state(s);
        state := *copy;
        option: Option = ---;
        option.pawn = get_pawn_from_position(state, o.pawn.position);
        option.new_position = o.new_position;

        move_pawn(state, option.pawn, option.new_position);
        options := get_valid_movements(state);
        total := 0.0;
        for option : options {
            value := zmovement(state, option);
            if value > 0  total += value;
        }

        return total;
    }

    options := get_valid_movements(state);
    //Basic.print("%\n", options);

    Value_Function :: #type (state: type_of(start_state), option: Option) -> float;
    //Value_Function :: type_of(forward);
    Function_Pair :: struct {
        function: Value_Function;
        weight: float64;
    }

    function_list := Function_Pair.[
        .{zmovement, 1000.0},
        .{lfc, -4624.85586054},
        .{tlfc, -116.075977679},
        .{ljet, 13.1453305679},
        .{tjet, 4.23700146139},
        .{ljnt, 343.057859556},
        .{tjnt, -0.0670862340581},
        .{bpawn, -143.870534048},
        .{density_x, -0.362336218878},
        .{density_y, -2.50429266667}
    ];

    best_option: Option;
    max_value := -9999999999.9; // I think -inf might exist

    if state.turns < 2 {
        if state.turn == RED
            return get_pawn_from_position(state, Position.{15, 3}), Position.{14, 4};
        else
            return get_pawn_from_position(state, Position.{15, 13}), Position.{14, 12};
    }

    for option : options {
        value : float64 = 0.0;
        for pair : function_list  value += pair.weight*pair.function(state, option);
        if value > max_value  best_option, max_value = option, value;
    }

    return best_option.pawn, best_option.new_position;
}