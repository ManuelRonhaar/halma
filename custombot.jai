Value_Function :: #type (state: type_of(start_state), option: Option) -> float;

Function_Info :: struct {
    function: Value_Function;
    weight: float;
    name: string;
}

Custom_Data :: struct {
    function_info: [4] Function_Info;
    fixed_start: bool = true;
    id: s64;
}

standard_custom_data : Custom_Data = .{
    .[
        .{forward, 5.0, "forward"},
        .{max_forward_next_turn, 3.0, "max forward next turn"},
        .{max_forward_opponent, -2.0, "max forward opponent"},
        .{standard_deviation, -6.0, "standard deviation"}
    ],
    true,
    0
};

custombot :: (state: *State($w, $h, $p), custom_data: Custom_Data) -> (pawn: *Pawn, destination: Position) {

    options := get_valid_movements(state);

    best_option: Option;
    max_value := 0hFF800000;; // -inf

    if state.turns < 2 {
        if state.turn == RED
            return get_pawn_from_position(state, Position.{15, 3}), Position.{14, 4};
        else
            return get_pawn_from_position(state, Position.{15, 13}), Position.{14, 12};
    }

    for option: options {
        value := 0.0;
        for info: custom_data.function_info  value += info.weight*info.function(state, option);
        if value > max_value  best_option, max_value = option, value;
    }

    return best_option.pawn, best_option.new_position;
}


forward :: (state: State_NP, option: Option) -> float {
    // steps to reach the outermost finish field
    steps_to_finish :: (position: Position, turn: s8) -> float {
        x_difference := abs(cast(float) (12-position.x));
        y_difference := ifx turn == RED then 16-position.y else position.y;
        return Basic.max(x_difference, cast(float) y_difference);
    }

    return steps_to_finish(option.pawn, state.turn) - steps_to_finish(option.new_position, state.turn);
}

standard_deviation :: (s: State_NP, o: Option) -> float {
    copy := copy_state(s);
    state := *copy;
    option: Option = ---;
    option.pawn = get_pawn_from_position(state, o.pawn.position);
    option.new_position = o.new_position;
    move_pawn(state, option.pawn, option.new_position);
    state.turn ^= 1;

    average := Vector2.{0.0, 0.0};
    for pawn: state.pawns_by_color[state.turn]  average += make_vector2(xx pawn.x, xx pawn.y);
    average.x /= cast(float) state.pawn_num;
    average.y /= cast(float) state.pawn_num;

    standard_deviation := 0.0;
    for pawn: state.pawns_by_color[state.turn]
        standard_deviation += Math.square(cast(float) pawn.x - average.x) + Math.square(Math.sqrt(3.0) * (cast(float) pawn.y - average.y));

    standard_deviation = Math.sqrt(standard_deviation / cast(float) state.pawn_num);
    return standard_deviation;
}

stay_centered :: (state: State_NP, option: Option) -> float {
    square :: x => x*x;
    extra_distance := cast(float) square(option.pawn.x - 12) - cast(float) square(option.new_position.x - 12);
    return extra_distance / 16;
}

max_forward_next_turn :: (s: State_NP, o: Option) -> float {
    copy := copy_state(s);
    state := *copy;
    option: Option = ---;
    option.pawn = get_pawn_from_position(state, o.pawn.position);
    option.new_position = o.new_position;

    move_pawn(state, option.pawn, option.new_position);
    state.turn ^= 1;
    options := get_valid_movements(state);
    max := 0.0;
    for option : options {
        value := forward(state, option);
        if value > max  max = value;
    }

    return max;
}

max_forward_opponent :: (s: State_NP, o: Option) -> float {
    copy := copy_state(s);
    state := *copy;
    option: Option = ---;
    option.pawn = get_pawn_from_position(state, o.pawn.position);
    option.new_position = o.new_position;

    move_pawn(state, option.pawn, option.new_position);
    options := get_valid_movements(state);
    max := 0.0;
    for option : options {
        value := forward(state, option);
        if value > max  max = value;
    }

    return max;
}